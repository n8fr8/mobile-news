#!/usr/bin/env sh

# abort on errors
set -e

# build
npm run build

# Build the workbox service worker manifest
workbox injectManifest workbox-config.js

# navigate into the build output directory
cd dist
cp -R * ../../public/
cd ..
