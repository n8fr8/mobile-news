/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");
importScripts("./dexie.js");

workbox.setConfig({
  debug: true
});

self.addEventListener('activate', event => {
  event.waitUntil(clients.claim());
});

const db = new Dexie("nuusdb");
db.version(1).stores({
  media: "url",
  items: "id"
});
db.version(2).stores(
  {
      feeds: "url"
  }
)
db.version(3).stores(
  {
      items: "id, feed"
  }
)
db.version(4).stores(
  {
      log_sw: "++id"
  }
)

const matchFeedRequest = ({url, event}) => {
  //console.log("TRYING TO MATCH " + url);
  if (url.href.endsWith(".xml")) {
    //console.log("*** FEED REQUEST: " + url);
    return true;
  }
  return false;
};

const handlerFeedRequest = ({url, event, params}) => {
  console.log("Inside handlerFeedRequest");
  const strategy = new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'feed-cache',
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
      new workbox.expiration.Plugin({
        maxAgeSeconds: 60 * 60 * 24 * 7, // A week
        maxEntries: 20
      }),
    ]
  });
  // Remove ths custom header
  // const headers = new Headers();
  // for (let [key, value] of event.request.headers) {
  //   console.log("Handling key " + key);
  //   if (key != 'x-destination') {
  //     console.log(`${key} = ${value}`);
  //     headers.append(key, value);
  //   }
  // }
  //const request = new Request(event.request, {headers: headers});
  return strategy.makeRequest({
    request: event.request
  });
};

console.log("Registering route!");
workbox.routing.registerRoute(matchFeedRequest, handlerFeedRequest);

// Cache the Google Fonts stylesheets with a stale-while-revalidate strategy.
workbox.routing.registerRoute(
  /^https:\/\/fonts\.googleapis\.com/,
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'google-fonts-stylesheets',
  })
);

// Cache the underlying font files with a cache-first strategy for 1 year...
workbox.routing.registerRoute(
  /^https:\/\/fonts\.gstatic\.com/,
  new workbox.strategies.CacheFirst({
    cacheName: 'google-fonts-webfonts',
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
      new workbox.expiration.Plugin({
        maxAgeSeconds: 60 * 60 * 24 * 365,
        maxEntries: 30,
      }),
    ],
  })
);

self.addEventListener('message', (event) => {
  console.log("SW - Got message:");
  console.log(event);
  if (event.data && event.data.type === 'SKIP_WAITING') {
    self.skipWaiting();
  } else if (event.data && event.data.type === 'FEED_URL_UPDATE') {
    console.log("Feed URL update:");
    const {updatedURLs} = event.data.payload;
    console.log(updatedURLs);
  } else if (event.data && event.data.type === 'GET_VERSION') {
    event.ports[0].postMessage(APP_VERSION_PLACEHOLDER);
  }
});

self.addEventListener('fetch', (e) => {
  //console.log("Feching " + e.request.url);
  e.respondWith(
      fetch(e.request)
          .then((response) => {
              if (response.ok || response.status == 0) return response;
              //console.log("ERROR FETCHING: " + e.request.url, response);
              db.log_sw.add({type:"fetch_error", url:e.request.url});
              return response;
          })
          .catch(error => {
            //console.error('EXCEPTION FETCHING: ', error, e.request.url);
        })
  )
});

self.addEventListener('sync', function(event) {
  console.log("Received syn event: " + event.tag);
  if (event.tag == 'feedUpdate') {
    console.log("SYNC1");
    event.waitUntil(
      console.log("SYNC!!!")
    );
  }
});

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});
