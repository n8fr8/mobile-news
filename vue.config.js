// Read version from package.json
const fs = require('fs')
const packageJson = fs.readFileSync('./package.json')
const version = JSON.parse(packageJson).version || 0

const webpack = require('webpack');

// Stamp service worked with current version string!
const fileNameIn = "service-worker-source.js";
const fileNameOut = "service-worker-versioned.js";
fs.readFile(fileNameIn, 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }
  var result = data.replace(/APP_VERSION_PLACEHOLDER/g, "'" + version + "'");

  fs.writeFile(fileNameOut, result, 'utf8', function (err) {
     if (err) return console.log(err);
  });
});

module.exports = {
    devServer: {
        proxy: {
            '^/rfa': {
                target: "https://www.rfa.org",
                changeOrigin: true,
                ws: true, // proxy websockets
                pathRewrite: {
                    '^/rfa': ''
                }
            }
        }
    },
    publicPath: process.env.NODE_ENV === 'production'
        ? './'
        : './',

    pwa: {
        workboxPluginMode: 'InjectManifest',
        workboxOptions: {
            swSrc: 'service-worker-versioned.js',
            swDest: 'service-worker.js'
        }
    },

    configureWebpack: {
        devtool: 'source-map',
        plugins: [
            new webpack.DefinePlugin({
                'process.env': {
                    PACKAGE_VERSION: '"' + version + '"'
                },
            })
        ]
    },

    lintOnSave: 'warning',
}
