export default {
    uyghur: {
        displayName: "Uyghur",
        name: "باش بەت",
        localeName: "ug",
        cssFile: "./assets/css/ug.css",
        webFontCssFile: "./assets/fonts/alpida.css",
        webFontConfig: {
            custom: {
                families: ["AlpidaUnicodeSystem"],
                testStrings: {
                    "AlpidaUnicodeSystem": "\u067E"
                }
            }
        },
        printFont: "./assets/fonts/alpdunisys.ttf",
        services: [
            {title: "Main service", url: "/uyghur/appcats/main/app_main_rss.xml", categories: [
                {url: "/uyghur/appcats/cat1/app_rss.xml"},
                {url: "/uyghur/appcats/cat2/app_rss.xml"},
                {url: "/uyghur/appcats/cat3/app_rss.xml"},
                {url: "/uyghur/appcats/cat4/app_rss.xml"},
                {url: "/uyghur/appcats/cat5/app_rss.xml"}
            ]}
        ],
        isRTL: true
    },
    uyghur_latin: {
        displayName: "Uyghur (latin encoding)",
        name: "Uyghurche",
        localeName: "ug_latin",
        cssFile: "./assets/css/ug.css",
        webFontCssFile: "./assets/fonts/alpida.css",
        webFontConfig: {
            custom: {
                families: ["AlpidaUnicodeSystem"],
                testStrings: {
                    "AlpidaUnicodeSystem": "\u067E"
                }
            }
        },
        printFont: "./assets/fonts/alpdunisys.ttf",
        services: [
            {title: "Main service", url: "/uyghur/appcats/main/app_main_rss.xml?encoding=latin", categories: [
                {url: "/uyghur/appcats/cat1/app_rss.xml?encoding=latin"},
                {url: "/uyghur/appcats/cat2/app_rss.xml?encoding=latin"},
                {url: "/uyghur/appcats/cat3/app_rss.xml?encoding=latin"},
                {url: "/uyghur/appcats/cat4/app_rss.xml?encoding=latin"},
                {url: "/uyghur/appcats/cat5/app_rss.xml?encoding=latin"}
            ]}
        ],
        isRTL: false
    },
    uyghur_cyrillic: {
        displayName: "Uyghur (cyrillic encoding)",
        name: "Уйғурчә",
        localeName: "ug_cyrillic",
        cssFile: "./assets/css/ug.css",
        webFontCssFile: "./assets/fonts/alpida.css",
        webFontConfig: {
            custom: {
                families: ["AlpidaUnicodeSystem"],
                testStrings: {
                    "AlpidaUnicodeSystem": "\u067E"
                }
            }
        },
        printFont: "./assets/fonts/alpdunisys.ttf",
        services: [
            {title: "Main service", url: "/uyghur/appcats/main/app_main_rss.xml?encoding=cyrillic", categories: [
                {url: "/uyghur/appcats/cat1/app_rss.xml?encoding=cyrillic"},
                {url: "/uyghur/appcats/cat2/app_rss.xml?encoding=cyrillic"},
                {url: "/uyghur/appcats/cat3/app_rss.xml?encoding=cyrillic"},
                {url: "/uyghur/appcats/cat4/app_rss.xml?encoding=cyrillic"},
                {url: "/uyghur/appcats/cat5/app_rss.xml?encoding=cyrillic"}
            ]}
        ],
        isRTL: false
    },
    mandarin: {
        displayName: "Mandarin",
        name: "普通话",
        localeName: "cmn",
        cssFile: "./assets/css/cmn.css",
        webFontCssFile: "./assets/fonts/yahei.css",
        webFontConfig: {
            custom: {
                families: ["Microsoft YaHei"],
                testStrings: {
                    "Microsoft YaHei": "\u4E00"
                }
            }
        },
        printFont: "./assets/fonts/msyh_0.ttf",
        services: [
            {title: "Main service", url: "/mandarin/appcats/main/app_main_rss.xml", categories: [
                {url: "/mandarin/appcats/cat1/app_rss.xml"},
                {url: "/mandarin/appcats/cat2/app_rss.xml"},
                {url: "/mandarin/appcats/cat3/app_rss.xml"},
                {url: "/mandarin/appcats/cat4/app_rss.xml"},
                {url: "/mandarin/appcats/cat5/app_rss.xml"}
            ]}
        ],
        isRTL: false
    },
    cantonese: {
        displayName: "Cantonese",
        name: "粤语部",
        localeName: "yue",
        cssFile: "./assets/css/yue.css",
        webFontCssFile: "./assets/fonts/NotoSansCJKtc.css",
        webFontConfig: {
            custom: {
                families: ["Noto Sans CJK TC"],
                testStrings: {
                    "Noto Sans CJK TC": "\u4E00"
                }
            }
        },
        printFont: "./assets/fonts/NotoSansCJKtc-Regular.otf",
        services: [
            {title: "Main service", url: "/cantonese/appcats/main/app_main_rss.xml?encoding=traditional", categories: [
                {url: "/cantonese/appcats/cat1/app_rss.xml?encoding=traditional"},
                {url: "/cantonese/appcats/cat2/app_rss.xml?encoding=traditional"},
                {url: "/cantonese/appcats/cat3/app_rss.xml?encoding=traditional"},
                {url: "/cantonese/appcats/cat4/app_rss.xml?encoding=traditional"},
                {url: "/cantonese/appcats/cat5/app_rss.xml?encoding=traditional"}
            ]}
        ],
        isRTL: false
    },
    burmese: {
        displayName: "Burmese",
        name: "ျမန္မာ",
        localeName: "my_MM",
        cssFile: "./assets/css/my_mm.css",
        webFontCssFile: "./assets/fonts/Pyidaungsu-2.5.3.css",
        webFontConfig: {
            custom: {
                families: ["Pyidaungsu"],
                testStrings: {
                    "Pyidaungsu": "\u1000"
                }
            }
        },
        printFont: "./assets/fonts/Pyidaungsu-2.5.3_Regular.ttf",
        services: [
            {title: "Main service", url: "/burmese/appcats/main/app_main_rss.xml", categories: [
                {url: "/burmese/appcats/cat1/app_rss.xml"},
                {url: "/burmese/appcats/cat2/app_rss.xml"},
                {url: "/burmese/appcats/cat3/app_rss.xml"},
                {url: "/burmese/appcats/cat4/app_rss.xml"},
                {url: "/burmese/appcats/cat5/app_rss.xml"}
            ]}
        ],
        isRTL: false
    },
    lao: {
        displayName: "Lao",
        name: "ລາວ",
        localeName: "lao",
        cssFile: "./assets/css/lao.css",
        webFontCssFile: "./assets/fonts/saysettha.css",
        webFontConfig: {
            custom: {
                families: ["Saysettha OT"],
                testStrings: {
                    "Saysettha OT": "\u0E81"
                }
            }
        },
        printFont: "./assets/fonts/saysettha.ttf",
        services: [
            {title: "Main service", url: "/lao/appcats/main/app_main_rss.xml", categories: [
                {url: "/lao/appcats/cat1/app_rss.xml"},
                {url: "/lao/appcats/cat2/app_rss.xml"},
                {url: "/lao/appcats/cat3/app_rss.xml"},
                {url: "/lao/appcats/cat4/app_rss.xml"},
                {url: "/lao/appcats/cat5/app_rss.xml"}
            ]}
        ],
        isRTL: false
    },
    korean: {
        displayName: "Korean",
        name: "한국의",
        localeName: "kor",
        cssFile: "./assets/css/kor.css",
        webFontCssFile: "./assets/fonts/NanumGothic.css",
        webFontConfig: {
            custom: {
                families: ["NanumGothic"],
                testStrings: {
                    "NanumGothic": "\uAC00"
                }
            }
        },
        printFont: "./assets/fonts/NanumGothic-Regular.ttf",
        services: [
            {title: "Main service", url: "/korean/appcats/main/app_main_rss.xml", categories: [
                {url: "/korean/appcats/cat1/app_rss.xml"},
                {url: "/korean/appcats/cat2/app_rss.xml"},
                {url: "/korean/appcats/cat3/app_rss.xml"},
                {url: "/korean/appcats/cat4/app_rss.xml"},
                {url: "/korean/appcats/cat5/app_rss.xml"}
            ]}
        ],
        isRTL: false
    },
    khmer: {
        displayName: "Khmer",
        name: "ខ្មែរ",
        localeName: "khm",
        cssFile: "./assets/css/khm.css",
        webFontCssFile: "./assets/fonts/Hanuman.css",
        webFontConfig: {
            custom: {
                families: ["Hanuman"],
                testStrings: {
                    "Hanuman": "\u1780"
                }
            }
        },
        printFont: "./assets/fonts/Hanuman-Regular.ttf",
        services: [
            {title: "Main service", url: "/khmer/appcats/main/app_main_rss.xml", categories: [
                {url: "/khmer/appcats/cat1/app_rss.xml"},
                {url: "/khmer/appcats/cat2/app_rss.xml"},
                {url: "/khmer/appcats/cat3/app_rss.xml"},
                {url: "/khmer/appcats/cat4/app_rss.xml"},
                {url: "/khmer/appcats/cat5/app_rss.xml"}
            ]}
        ],
        isRTL: false
    },
    vietnamese: {
        displayName: "Vietnamese",
        name: "Tiếng Việt",
        localeName: "vi_VN",
        cssFile: "./assets/css/vi_vn.css",
        webFontCssFile: "./assets/fonts/roboto.css",
        webFontConfig: {
            google: {
                families: ['Roboto']
            }
        },
        printFont: "./assets/fonts/Roboto-Regular.ttf",
        services: [
            {title: "Main service", url: "/vietnamese/appcats/main/app_main_rss.xml", categories: [
                {url: "/vietnamese/appcats/cat1/app_rss.xml"},
                {url: "/vietnamese/appcats/cat2/app_rss.xml"},
                {url: "/vietnamese/appcats/cat3/app_rss.xml"},
                {url: "/vietnamese/appcats/cat4/app_rss.xml"},
                {url: "/vietnamese/appcats/cat5/app_rss.xml"}
            ]}
        ],
        isRTL: false
    },
    tibetan: {
        displayName: "Tibetan",
        name: "བོད་སྐད།",
        localeName: "bo",
        cssFile: "./assets/css/bo.css",
        webFontCssFile: "./assets/fonts/monlam.css",
        webFontConfig: {
            custom: {
                families: ["Monlam Uni Ouchan2"],
                testStrings: {
                    "Monlam Uni Ouchan2": "\u0F00"
                }
            }
        },
        printFont: "./assets/fonts/Monlam_Uni_Ochan2.ttf",
        services: [
            {title: "Main service", url: "/tibetan/appcats/main/app_main_rss.xml", categories: [
                {url: "/tibetan/appcats/cat1/app_rss.xml"},
                {url: "/tibetan/appcats/cat2/app_rss.xml"},
                {url: "/tibetan/appcats/cat3/app_rss.xml"},
                {url: "/tibetan/appcats/cat4/app_rss.xml"},
                {url: "/tibetan/appcats/cat5/app_rss.xml"}
            ]}
        ],
        isRTL: false
    },
    english: {
        displayName: "English",
        name: "English",
        localeName: "en",
        cssFile: "./assets/css/default.css",
        webFontCssFile: "./assets/fonts/roboto.css",
        webFontConfig: {
            google: {
                families: ['Roboto']
            }
        },
        services: [
            {title: "Main service", url: "/english/appcats/main/app_main_rss.xml", categories: [
                {url: "/english/appcats/cat1/app_rss.xml"},
                {url: "/english/appcats/cat2/app_rss.xml"},
                {url: "/english/appcats/cat3/app_rss.xml"},
                {url: "/english/appcats/cat4/app_rss.xml"},
                {url: "/english/appcats/cat5/app_rss.xml"}
            ], audioFeedUrl: "./assets/radio/feed.xml"}
        ],
        isRTL: false
    }
};
