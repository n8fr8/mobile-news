import Vue from 'vue'
import Router from 'vue-router'
import Main from './views/Main.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Main,
      children: [
        {
          path: '',
          name: 'home',
          component: () => import('./views/Home.vue'),
          props: true,
          meta: {tab:0}
        },
        {
          path: '/categories',
          name: 'categories',
          component: () => import('./views/Home.vue'),
          props: { headerType: 'categories'},
          meta: {tab:1}
        },
        {
          path: '/radio',
          name: 'radio',
          component: () => import('./views/Radio.vue'),
          props: true,
          meta: {tab:2}
        },
        {
          path: '/saved',
          name: 'saved',
          component: () => import('./views/Home.vue'),
          props: { headerType: 'saved' },
          meta: {tab:3}
        },
        {
          path: '/more',
          name: 'more',
          component: () => import('./views/More.vue'),
          props: true,
          meta: {tab:4}
        }
      ]
    },
    {
      path: '/onboarding',
      name: 'onboarding',
      component: () => import('./views/Onboarding.vue')
    }
  ]
})
