import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

Vue.use(Vuex)

const vuexPersist = new VuexPersist({
  key: 'settings',
  storage: localStorage,
  reducer: state => ({
    onboarded: state.onboarded,
    showMedia: state.showMedia,
    flavor: state.flavor,
    textSizeAdjustment: state.textSizeAdjustment,
    currentAppVersion: state.currentAppVersion
  })
})

export default new Vuex.Store({
  state: {
    onboarded: false,
    showMedia: false,
    flavor: "unknown",
    currentAppVersion: null, // Last known version we ran. Used for displaying "you just updated to ..."
    textSizeAdjustment: 0,
    currentFeedTitle: "",
    currentFeedItems: [],
    currentFeedCategories: [],
    currentFeedCategoriesWithItems: [],
    currentFeedAudio: null,
    fullScreenItems: null,
    fullScreenItemIndex: -1,
    liveRadio: null,
    radioSchedule: [],
    isLandscapeMode: false,
    showingFullScreenVideo: false
  },
  mutations: {
    onboarded (state, onboarded) {
      state.onboarded = onboarded;
    },
    showMedia (state, value) {
      state.showMedia = value;
    },
    setFlavor(state, flavor) {
      state.flavor = flavor;
    },
    setCurrentAppVersion(state, version) {
      state.currentAppVersion = version;
    },
    setTextSizeAdjustment(state, adjustment) {
      state.textSizeAdjustment = adjustment;
    },
    setCurrentFeedTitle(state, title) {
      state.currentFeedTitle = title;
    },
    setCurrentFeedItems(state, items) {
      state.currentFeedItems = items;
    },
    clearCategories(state, numCategories) {
      state.currentFeedCategories = [];
      state.currentFeedCategoriesWithItems = [];
      for (var i = 0; i < numCategories; i++) {
        state.currentFeedCategories.push({feed:null,items:null});
      }
    },
    addCategoryItems(state, category) {
      Object.assign(state.currentFeedCategories[category.index], category);
      state.currentFeedCategoriesWithItems = state.currentFeedCategories.filter(function(i) {
        return i.items != null;
      });
    },

    /**
     * Set audio feed info.
     * @param {*} state 
     * @param {*} currentFeedAudio Should contain info for current feed audio. Either null or an object of {feed: {}, items: []}.
     */
    setCurrentFeedAudio(state, currentFeedAudio) {
      console.log("Sett current feed audio to: ");
      console.log(currentFeedAudio);
      state.currentFeedAudio = currentFeedAudio;
    },
    setLiveRadio(state, radio) {
        state.liveRadio = radio;
    },
    setRadioSchedule(state, broadcasts) {
      state.radioSchedule = broadcasts;
    },
    setFullScreenItems(state, items) {
      state.fullScreenItems = items;
    },
    setFullScreenItemIndex(state, index) {
      state.fullScreenItemIndex = index;
    },
    setLandscapeMode(state, isLandscapeMode) {
      state.isLandscapeMode = isLandscapeMode;
    },
    showingFullScreenVideo(state, showingFullScreenVideo) {
      state.showingFullScreenVideo = showingFullScreenVideo;
    }
 },
  plugins: [vuexPersist.plugin]
})
