import Vue from 'vue'
import VueI18n from 'vue-i18n'

import en from './translations/en.json';
import ug from './translations/ug.json';
import ug_latin from './translations/ug_latin.json';
import ug_cyrillic from './translations/ug_cyrillic.json';
import cmn from './translations/cmn.json';
import yue from './translations/yue.json';
import my_MM from './translations/my_MM.json';
import lao from './translations/lao.json';
import kor from './translations/kor.json';
import khm from './translations/khm.json';
import vi_VN from './translations/vi_VN.json';
import bo from './translations/bo.json';

Vue.use(VueI18n)

export default new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  silentFallbackWarn: true,
  messages: {
    en: en,
    ug: ug,
    ug_latin: ug_latin,
    ug_cyrillic: ug_cyrillic,
    cmn: cmn,
    yue: yue,
    my_MM: my_MM,
    lao: lao,
    kor: kor,
    khm: khm,
    vi_VN: vi_VN,
    bo: bo
  }
})